﻿using AT_Framework_Task_1_v_1.Loger;
using AT_Framework_Task_1_v_1.Models;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.Extensions;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.PageObjects;
using SeleniumExtras.WaitHelpers;

namespace AT_Framework_Task_1_v_1.Pages
{
    public class GoogleCloudCalculator
    {
        private const string _inputTotalPrice = "$5,628.90";
        private const string _inputGPUModelField = "GPU Model";
        private const string _inputNumberOfGPUField = "Number of GPUs";
        private const string _inputCurrencySymbol = "USD";
        private const string _inputExpectedPerTime = "/ month";
        
        [FindsBy(How = How.ClassName, Using = "UywwFc-vQzf8d")]
        private readonly IWebElement? _addToEstimate;

        [FindsBy(How = How.Id, Using = "c2")]
        private readonly IWebElement? _searchComputeEngine;

        [FindsBy(How = How.ClassName, Using = "BPgnDc")]
        private readonly IWebElement? _computeEngineButton;

        [FindsBy(How = How.Id, Using = "c126")]
        private readonly IWebElement? _nameOfServiceType;

        [FindsBy(How = How.Id, Using = "c11")]
        private readonly IWebElement? _numberOfInstance;

        [FindsBy(How = How.Id, Using = "c19")]
        private readonly IWebElement? _operationSystem;

        [FindsBy(How = How.Id, Using = "c26")]
        private readonly IWebElement? _machineFamily;

        [FindsBy(How = How.Id, Using = "c30")]
        private readonly IWebElement? _series;

        [FindsBy(How = How.Id, Using = "c33")]
        private readonly IWebElement? _machineType;

        [FindsBy(How = How.CssSelector, Using = "div.AsBIyb button[aria-label='Add GPUs']")]
        private readonly IWebElement? _addGPUButton;

        [FindsBy(How = How.XPath, Using = $"//span[text()='{_inputGPUModelField}']")]
        private readonly IWebElement? _gPUModel;

        [FindsBy(How = How.XPath, Using = $"//span[text()='{_inputNumberOfGPUField}']")]
        private readonly IWebElement? _numberOfGPUs;

        [FindsBy(How = How.Id, Using = "c42")]
        private readonly IWebElement? _localSSD;

        [FindsBy(How = How.Id, Using = "c46")]
        private readonly IWebElement? _region;

        [FindsBy(How = How.XPath, Using = "//span[text()='Add to estimate']")]
        private readonly IWebElement? _addToEstimateButton;

        [FindsBy(How = How.XPath, Using = "//button[@data-idom-class='bwApif-zMU9ub']")]
        private readonly IWebElement? _closeEstimateButton;

        [FindsBy(How = How.ClassName, Using = "FOBRw-vQzf8d")]
        private readonly IWebElement? _shareButton;

        [FindsBy(How = How.CssSelector, Using = "[track-name='open estimate summary']")]
        private readonly IWebElement? _openEstimateSummaryButton;

        [FindsBy(How = How.XPath, Using = "//span[@class='MyvX5d D0aEmf']")]
        private readonly IWebElement? _amount;

        [FindsBy(How = How.CssSelector, Using = "span.AeBiU-vQzf8d")]
        private readonly IWebElement? _existCurrencyElement;

        [FindsBy(How = How.ClassName, Using = "WUm90")]
        private readonly IWebElement? _existPerTime;

        private readonly IWebDriver? _driver;
        
        public GoogleCloudCalculator(IWebDriver driver)
        {
            this._driver = driver;
            PageFactory.InitElements(_driver, this);
        }
        #region Business method
        public void OpenPage(EstimateGoogleCloudPrice estimatePrice)
        {
            Logger.Info($"Open {estimatePrice.Calculator_URL}");
            _driver?.Navigate().GoToUrl(estimatePrice.Calculator_URL);
        }
        public void AddToEstimate()
        {
            Logger.Info($"Press button Add to estimate");
            _addToEstimate?.Click();
        }
        public void SearchField(EstimateGoogleCloudPrice estimatePrice)
        {
            Logger.Info($"Search {estimatePrice.SearchedEntities}");
            WaitVisibleById("c2");
            _searchComputeEngine?.SendKeys(estimatePrice.SearchedEntities);
            _computeEngineButton?.Click();
        }
        public void SetServiceType(EstimateGoogleCloudPrice estimatePrice)
        {
            Logger.Info($"Set service type {estimatePrice.ServiceType}");
            WaitVisibleById("c126");
            ClickJS(_nameOfServiceType);
            ClickJS(FindElementBySpanText(estimatePrice.ServiceType));
        }

        public void AddNumberOfInstance(EstimateGoogleCloudPrice estimatePrice)
        {
            Logger.Info($"Set number of instance {estimatePrice.InstancesNumber}");
            _numberOfInstance?.Clear();
            _numberOfInstance?.SendKeys($"{estimatePrice.InstancesNumber}");
        }

        public void ChoiceOperationSystem(EstimateGoogleCloudPrice estimatePrice)
        {
            Logger.Info($"Set operation system {estimatePrice.OperationSystem}");
            ClickJS(_operationSystem);
            FindByXpathContain(estimatePrice.OperationSystem)?.Click();
        }
        public void ChoiceProvisioningModel(EstimateGoogleCloudPrice estimatePrice)
        {
            Logger.Info($"Choice provision model {estimatePrice.ProvisionModel}");
            ScroolJS(FindById(estimatePrice.ProvisionModel));
            ClickJS(FindById(estimatePrice.ProvisionModel));
        }
        public void ChoiceMachineFamily(EstimateGoogleCloudPrice estimatePrice)
        {
            Logger.Info($"Choice machine family {estimatePrice.MachineFamily}");
            ClickJS(_machineFamily);
            FindByXpathContain(estimatePrice.MachineFamily)?.Click();
        }
        public void ChoiceSeries(EstimateGoogleCloudPrice estimatePrice)
        {
            Logger.Info($"Choice series {estimatePrice.SeriesValue}");
            ClickJS(_series);
            FindByXpathContain(estimatePrice.SeriesValue)?.Click();
        }
        public void ChoiceMachineType(EstimateGoogleCloudPrice estimatePrice)
        {
            Logger.Info($"Choice machine type {estimatePrice.MachineTypeValue}");
            ClickJS(_machineType);
            FindByXpathContain(estimatePrice.MachineTypeValue)?.Click();
        }
        public void TurnAddGPUButton()
        {
            Logger.Info($"Turn on Add GPU");
            _addGPUButton?.Click();
            VerifyTurnAddGPUButtonIsTunedOn();
        }
        public void ChoiceGPUModel(EstimateGoogleCloudPrice estimatePrice)
        {
            Logger.Info($"Choice GPU model {estimatePrice.GPUModelValue}");
            WaitVisibleByXPath("//span[text()='GPU Model']");
            ClickJS(_gPUModel);
            FindByXpathContain(estimatePrice.GPUModelValue)?.Click();
        }
        public void ChoiceNumberOfGPUs(EstimateGoogleCloudPrice estimatePrice)
        {
            Logger.Info($"Choice number of GPU's {estimatePrice.NumberOfGPUValue}");
            ClickJS(_numberOfGPUs);
            ClickJS(FindByXpathValue(estimatePrice.NumberOfGPUValue));
        }
        public void ChoiceLocalSSD(EstimateGoogleCloudPrice estimatePrice)
        {
            Logger.Info($"Choice local SSD {estimatePrice.LocalSSDValue}");
            ClickJS(_localSSD);
            FindByXpathLiContain(estimatePrice.LocalSSDValue)?.Click();
        }
        public void ChoiceRegion(EstimateGoogleCloudPrice estimatePrice)
        {
            Logger.Info($"Choice region {estimatePrice.RegionValue}");
            ClickJS(_region);
            FindByXpathContain(estimatePrice.RegionValue)?.Click();
        }
        public void ChoiceCommitedUsage(EstimateGoogleCloudPrice estimatePrice)
        {
            Logger.Info($"Choice usage time {estimatePrice.UsageTime}");
            FindByXpathContainText(estimatePrice.UsageTime)?.Click();
            VerifyCurrencyIsUSD();
        }
        public void CountTotalEstimate()
        {
            _addToEstimateButton?.Click();
            _closeEstimateButton?.Click();
            WaitTextPresent(_amount, _inputTotalPrice);
            VerifyTotalEstimate();
            VerifyPerTimeUsage();
        }
        public void ShareEstimate()
        {
            ClickJS(_shareButton);
        }
        public void OpenEstimateSummary()
        {
            ClickJS(_openEstimateSummaryButton);
        }
        #endregion
        #region Waiters
        public void WaitVisibleByXPath(string path)
        {
            WebDriverWait wait = new(_driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(path)));
        }
        public void WaitVisibleById(string id)
        {
            WebDriverWait wait = new(_driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id(id)));
        }
        public void WaitExistByClassName(string path)
        {
            WebDriverWait wait = new(_driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.ElementExists(By.ClassName(path)));
        }
        public void WaitTextPresent(IWebElement? element, string expectedText)
        {
            WebDriverWait wait = new(_driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.TextToBePresentInElement(element, expectedText));
        }
        #endregion
        #region JS
        public void ClickJS(IWebElement? element)
        {
            _driver.ExecuteJavaScript("arguments[0].click();", element);
        }
        public void ScroolJS(IWebElement? element)
        {
            _driver.ExecuteJavaScript("arguments[0].scrollIntoView(true);", element);
        }
        #endregion

        public void Click(IWebElement element)
        {
            element.Click();
        }
        public void SendKeys(IWebElement element, string inputText)
        {
            element.SendKeys(inputText);
        }
        public void SwitchBrowserWindow(int tabNumber)
        {
            _driver.SwitchTo().Window(_driver.WindowHandles[tabNumber]);
        }
        #region Verify method
        public void VerifyTurnAddGPUButtonIsTunedOn()
        {
            Assert.That(_addGPUButton?.GetAttribute("aria-checked"), Is.EqualTo("true"), "Switch is not turned on.");
        }
        public void VerifyCurrencyIsUSD()
        {
            Assert.That(_existCurrencyElement?.Text, Is.EqualTo(_inputCurrencySymbol), "Currency is not USD");
        }
        public void VerifyTotalEstimate()
        {
            Assert.That(_amount?.Text, Is.EqualTo(_inputTotalPrice), $"Price not equal {_inputTotalPrice}");
        }
        public void VerifyPerTimeUsage()
        {
            Assert.That(_existPerTime?.Text, Is.EqualTo(_inputExpectedPerTime), "Price are not for per month");
        }
        #endregion
        #region Driver method
        public IWebElement FindByXpathContain(string? element)
        {
            return _driver.FindElement(By.XPath($"//li[contains(@data-value, '{element}')]"));
        }
        public IWebElement FindById(string? id)
        {
            return _driver.FindElement(By.Id($"{id}"));
        }
        public IWebElement FindElementBySpanText(string? text)
        {
            return _driver.FindElement(By.XPath($"//span[text()='{text}']"));
        }
        public IWebElement FindByXpathValue(string? element)
        {
            return _driver.FindElement(By.XPath($"//li[@data-value='{element}']"));
        }
        public IWebElement FindByXpathContainText(string? text)
        {
            return _driver.FindElement(By.XPath($"//label[contains(text(), '{text}')]"));
        }
        public IWebElement FindByXpathLiContain(string? text)
        {
            return _driver.FindElement(By.XPath($"//li[contains(., '{text}')]"));
        }
        #endregion
    }
}
