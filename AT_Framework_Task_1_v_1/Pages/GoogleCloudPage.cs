﻿using AT_Framework_Task_1_v_1.Driver;
using AT_Framework_Task_1_v_1.Loger;
using AT_Framework_Task_1_v_1.Models;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.PageObjects;
using SeleniumExtras.WaitHelpers;

namespace AT_Framework_Task_1_v_1.Pages
{
    public class GoogleCloudPage
    {
        [FindsBy(How = How.ClassName, Using = "YSM5S")]
        private IWebElement? searchIcon;

        [FindsBy(How = How.ClassName, Using = "qdOxv-fmcmS-wGMbrd")]
        private IWebElement? inputSearchField;

        [FindsBy(How = How.CssSelector, Using = ".PETVs.PETVs-OWXEXe-UbuQg")]
        private IWebElement? pressSearchButton;

        [FindsBy(How = How.ClassName, Using = "K5hUy")]
        private IWebElement? searchResultElement;

        private readonly IWebDriver _driver;

        public GoogleCloudPage(IWebDriver driver)
        {
            this._driver = driver;
            PageFactory.InitElements(this._driver, this);
        }

        public void OpenPage(GoogleCloudSearchPage searchPage)
        {
            Logger.Info($"Open {searchPage.SearchURL}");
            _driver.Navigate().GoToUrl(searchPage.SearchURL);
        }

        public void GoogleCloudSearchCalculator(GoogleCloudSearchPage searchPage)
        {
            Logger.Info($"Start search Google Cloud Platform Pricing Calculator");
            searchIcon?.Click();
            Logger.Info($"Past value {searchPage.SearchKey}");
            inputSearchField?.SendKeys(searchPage.SearchKey);
            pressSearchButton?.Click();
            inputSearchField?.Click();
            WebDriverWait wait = new(_driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("K5hUy")));
            searchResultElement?.Click();
            Logger.Info($"Page open");
        }        
    }
}
