﻿using System.Drawing;
using System.Drawing.Imaging;
using OpenQA.Selenium;

namespace AT_Framework_Task_1_v_1.Screenshots
{
    internal static class ScreenshotTaker
    {
        public static string ScreenShotPath => @"E:\Study\EPAM\Automated Testing\AT_Framework_singelton\AT_Framework_Task_1_v_1\TestFail_Screenshots\";
        internal static void TakeScreenshot(IWebDriver? driver, string? testName, string? folderPath)
        {
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }

            string screenFileName =
                $"{testName} {DateTime.Now:dd,MM,yyyy_H,mm,ss}.{ImageFormat.Jpeg.ToString().ToLowerInvariant()}";

            string screenPath = Path.Combine(ScreenShotPath, screenFileName);

            using (Image screenshot = Image.FromStream(new MemoryStream(((ITakesScreenshot)driver).GetScreenshot().AsByteArray)))
            {
                screenshot?.Save(screenPath);
            }
        }
    }
}
