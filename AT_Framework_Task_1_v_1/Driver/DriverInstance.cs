﻿using OpenQA.Selenium.Firefox;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Chrome;
using NUnit.Framework.Interfaces;
using Microsoft.TeamFoundation.TestManagement.WebApi;
using AT_Framework_Task_1_v_1.Steps;
using AT_Framework_Task_1_v_1.Loger;

namespace AT_Framework_Task_1_v_1.Driver
{
    public class DriverInstance
    {
        private static ChromeDriver? _driver;

        private DriverInstance() { }

        public static IWebDriver GetInstance()
        {
            if (_driver == null)
            {
                _driver = new ChromeDriver();
                _driver.Manage().Timeouts().ImplicitWait.Add(TimeSpan.FromSeconds(30));
                _driver.Manage().Window.Maximize();
            }
            return _driver;
        }

        public static void CloseBrowser()
        {            
            _driver?.Quit();
            _driver = null;
        }
    }
}
