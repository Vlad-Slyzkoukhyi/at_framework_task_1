﻿using AT_Framework_Task_1_v_1.Models;

namespace AT_Framework_Task_1_v_1.Environment
{
    public class EnvironmentConfig
    {
        public string? BaseUrl { get; set; }
        public EstimateGoogleCloudPrice? Estimation { get; set; }
        public GoogleCloudSearchPage? SearchPage { get; set; }
    }
}
