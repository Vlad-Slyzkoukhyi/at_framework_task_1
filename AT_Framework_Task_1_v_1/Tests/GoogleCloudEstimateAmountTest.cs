using AT_Framework_Task_1_v_1.Loger;
using AT_Framework_Task_1_v_1.Pages;
using AT_Framework_Task_1_v_1.Screenshots;
using Microsoft.TeamFoundation.TestManagement.WebApi;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;


namespace AT_Framework_Task_1_v_1.Tests
{
    [TestFixture]
    public class GoogleCloudEstimateAmountTest
    {
        private Steps.Steps steps = new Steps.Steps();

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            Logger.InitLogger(TestContext.CurrentContext.Test.Name, TestContext.CurrentContext.TestDirectory);
        }

        [SetUp]
        public void Init()
        {
            Logger.Info("Test begin");
            steps.InitBrowser();
        }

        [Test]
        public void CalculateTotalEstimateGoogleCloud()
        {
            Logger.Info("Calculate Estimate test method begin");
            steps.CalculateEstimateGoogleCloud();
        }
        
        [Test]
        public void GoogleCloudSearch()
        {
            Logger.Info("Open Google Cloud test method begin");
            steps.OpenGoogleCloud();
        }        

        [TearDown]
        public void Cleanup()
        {
            if (TestContext.CurrentContext.Result.Outcome.Status == NUnit.Framework.Interfaces.TestStatus.Failed)
            {
                Logger.Info("Test is failed");
                steps.SaveScreenshoot(TestContext.CurrentContext.Test.MethodName, Path.Combine(TestContext.CurrentContext.TestDirectory, ScreenshotTaker.ScreenShotPath));
            }
            Logger.Info("Test End");
            steps.CloseBrowser();
        }
    }
}