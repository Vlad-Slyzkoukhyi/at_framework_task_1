﻿using Microsoft.VisualStudio.Services.Profile;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT_Framework_Task_1_v_1.Tests
{
    public class Assertions
    {
        [FindsBy(How = How.XPath, Using = "//span[contains(text(), 'Number of Instances')]/following-sibling::span[@class='Kfvdz']")]
        private readonly IWebElement? _existedNumberOfIstance;

        [FindsBy(How = How.XPath, Using = "//span[contains(text(), 'Operating System / Software')]/following-sibling::span[@class='Kfvdz']")]
        private readonly IWebElement? _existedOperationSystem;

        [FindsBy(How = How.XPath, Using = "//span[contains(text(), 'Provisioning Model')]/following-sibling::span[@class='Kfvdz']")]
        private readonly IWebElement? _existedProvisionModel;

        [FindsBy(How = How.XPath, Using = "//span[contains(text(), 'Machine type')]/following-sibling::span[@class='Kfvdz']")]
        private readonly IWebElement? _existedMachineType;

        [FindsBy(How = How.XPath, Using = "//span[contains(text(), 'GPU Model')]/following-sibling::span[@class='Kfvdz']")]
        private readonly IWebElement? _existedGPUModel;

        [FindsBy(How = How.XPath, Using = "//span[contains(text(), 'Number of GPUs')]/following-sibling::span[@class='Kfvdz']")]
        private readonly IWebElement? _existedNumberOfGPU;

        [FindsBy(How = How.XPath, Using = "//span[contains(text(), 'Local SSD')]/following-sibling::span[@class='Kfvdz']")]
        private readonly IWebElement? _existedLocalSSD;

        [FindsBy(How = How.XPath, Using = "//span[contains(text(), 'Region')]/following-sibling::span[@class='Kfvdz']")]
        private readonly IWebElement? _existedRegion;

        [FindsBy(How = How.XPath, Using = "//span[contains(text(), 'Committed use discount options')]/following-sibling::span[@class='Kfvdz']")]
        private readonly IWebElement? _existedCommitedUsage;

        [FindsBy(How = How.CssSelector, Using = "p.EOyc0b.HY0Uh")]
        private readonly IWebElement? _existedCurrency;

        [FindsBy(How = How.CssSelector, Using = "span.zv7tnb.W42XEf + span.OtcLZb.OIcOye")]
        private readonly IWebElement? _existedAmount;

        [FindsBy(How = How.CssSelector, Using = "h4.n8xu5 + span.hl8E4b")]
        private readonly IWebElement? _existedPerTime;

        [FindsBy(How = How.Id, Using = "ucj-1")]
        private readonly IWebElement? _googleAddToEstimatePage;

        private readonly IWebDriver _driver;
        public Assertions(IWebDriver driver)
        {
            this._driver = driver;
            PageFactory.InitElements(this._driver, this);
        }

        private const string _inputInstancesNumber = "4";
        private const string _inputTotalPrice = "$5,628.90";
        private const string _inputLocalSSDValue = "2x375 GB";
        private const string _inputRegionValue = "europe-west4";
        private const string _expectedOperationSystem = "Free: Debian, CentOS, CoreOS, Ubuntu or BYOL (Bring Your Own License)";
        private const string _expectedProvisioningModel = "Regular";
        private const string _expectedMachineType = "n1-standard-8, vCPUs: 8, RAM: 30 GB";
        private const string _expectedGPUModel = "NVIDIA Tesla V100";
        private const string _inputNumberOfGPUValue = "1";
        private const string _inputUsageTime = "1 year";
        private const string _inputCurrencySymbol = "USD";
        private const string _inputExpectedPerTime = "/ mo";

        
        public void VerifyAddToEstimatePageDispalyed()
        {
            Assert.That(_googleAddToEstimatePage.Displayed, Is.True, "Page does not open");
        }
        public void VerifyInstanceNumber()
        {
            Assert.That(_existedNumberOfIstance?.Text, Is.EqualTo(_inputInstancesNumber), 
                $"Number of istance is not {_inputInstancesNumber} as expected");
        }
        public void VerifyOperationSystem()
        {
            Assert.That(_existedOperationSystem?.Text, Does.Contain(_expectedOperationSystem), 
                $"Operation system is not {_expectedOperationSystem} as expected");
        }
        public void VerifyProvisioningModel()
        { 
            Assert.That(_existedProvisionModel?.Text, Is.EqualTo(_expectedProvisioningModel), 
                $"Provisioning Model is not {_expectedProvisioningModel} as expected");
        }
        public void VerifyMachineType()
        {
            Assert.That(_existedMachineType?.Text, Is.EqualTo(_expectedMachineType), 
                $"Machine type is not {_expectedMachineType} as expected");
        }
        public void VerifyGPUModel()
        {
            Assert.That(_existedGPUModel?.Text, Is.EqualTo(_expectedGPUModel), $"GPU Model is not {_expectedGPUModel} as expected");
        }
        public void VerifyNumberofGPU()
        {
            Assert.That(_existedNumberOfGPU?.Text, Is.EqualTo(_inputNumberOfGPUValue), $"Number of GPU is not {_inputNumberOfGPUValue} as expected");
        }
        public void VerifyLocalSSD()
        {
            Assert.That(_existedLocalSSD?.Text, Is.EqualTo(_inputLocalSSDValue), $"Number of GPU is not {_inputLocalSSDValue} as expected");
        }
        public void VerifyRegion()
        {
            Assert.That(_existedRegion?.Text, Does.Contain(_inputRegionValue), $"Region is not {_inputRegionValue} as expected");
        }
        public void VerifyCommitedUsage()
        {
            Assert.That(_existedCommitedUsage?.Text, Is.EqualTo(_inputUsageTime), $"Number of GPU is not {_inputUsageTime} as expected");
        }
        public void VerifyCurrency()
        {
            Assert.That(_existedCurrency?.Text, Does.Contain(_inputCurrencySymbol), $"Currency is not {_inputCurrencySymbol} as expected");
        }
        public void VerifyTotalAmount()
        {
            Assert.That(_existedAmount?.Text, Is.EqualTo(_inputTotalPrice), $"Total price is not {_inputTotalPrice} as expected");
        }
        public void VerifyExploitaionTime()
        {
            Assert.That(_existedPerTime?.Text, Is.EqualTo(_inputExpectedPerTime), $"Paid is not per {_inputExpectedPerTime} as expected");
        }
    }
}
