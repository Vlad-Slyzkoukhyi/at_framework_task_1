﻿namespace AT_Framework_Task_1_v_1.Models
{
    public class GoogleCloudSearchPage
    {
        public string? SearchURL { get; set; }
        public string? SearchKey { get; set; }
        public GoogleCloudSearchPage() { }

        public GoogleCloudSearchPage(string searchURL, string searchKey)
        {
            SearchURL = searchURL;
            SearchKey = searchKey;
        }       
    }
}
