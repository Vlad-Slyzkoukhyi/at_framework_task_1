﻿namespace AT_Framework_Task_1_v_1.Models
{
    public class EstimateGoogleCloudPrice
    {
        public string? Calculator_URL { get; set; }
        public string? SearchedEntities { get; set; }
        public string? ServiceType { get; set; }
        public string? MachineFamily { get; set; }
        public string? SearchingValue { get; set; }
        public string? InstancesNumber { get; set; }
        public string? OperationSystem { get; set; }
        public string? ProvisionModel { get; set; }
        public string? SeriesValue { get; set; }
        public string? MachineTypeValue { get; set; }
        public string? GPUModelValue { get; set; }
        public string? LocalSSDValue { get; set; }
        public string? RegionValue { get; set; }
        public string? NumberOfGPUValue { get; set; }
        public string? UsageTime { get; set; }

        public EstimateGoogleCloudPrice() { }
        public EstimateGoogleCloudPrice(string calculatorURL, string searchedEntities, string seviceType, string machineFamily, string searchingValue, string instancesNumber,
            string operationSystem, string provisionModel, string seriesValue, string machineTypeValue, string gPUModelValue, string localSSDValue,
            string regionValue, string numberOfGPUValue, string usageTime)
        {
            Calculator_URL = calculatorURL;
            SearchedEntities = searchedEntities;
            ServiceType = seviceType;
            MachineFamily = machineFamily;
            SearchingValue = searchingValue;
            InstancesNumber = instancesNumber;
            OperationSystem = operationSystem;
            ProvisionModel = provisionModel;
            SeriesValue = seriesValue;
            MachineTypeValue = machineTypeValue;
            GPUModelValue = gPUModelValue;
            LocalSSDValue = localSSDValue;
            RegionValue = regionValue;
            NumberOfGPUValue = numberOfGPUValue;
            UsageTime = usageTime;
        }
    }
}
