﻿using AT_Framework_Task_1_v_1.Environment;
using AT_Framework_Task_1_v_1.Loger;
using AT_Framework_Task_1_v_1.Models;
using AT_Framework_Task_1_v_1.Screenshots;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.Services.Account;
using OpenQA.Selenium;


namespace AT_Framework_Task_1_v_1.Steps
{
    public class Steps
    {
        private IWebDriver? _driver;
        private EnvironmentConfig? _config;
        
        public void InitBrowser()
        {
            _driver = Driver.DriverInstance.GetInstance();
            LoadConfiguration("Staging");
        }
        private void LoadConfiguration(string environment)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

            IConfigurationRoot configuration = builder.Build();
            _config = new EnvironmentConfig();
            configuration.GetSection(environment).Bind(_config);
        }

        public void CloseBrowser()
        {
            Driver.DriverInstance.CloseBrowser();
        }

        public void OpenGoogleCloud()
        {
            Pages.GoogleCloudPage googleCloudPage = new Pages.GoogleCloudPage(_driver);
            GoogleCloudSearchPage googleSearchPage = _config.SearchPage;
            Tests.Assertions assertions = new Tests.Assertions(_driver);
            googleCloudPage.OpenPage(googleSearchPage);
            googleCloudPage.GoogleCloudSearchCalculator(googleSearchPage);
            assertions.VerifyAddToEstimatePageDispalyed();
        }

        public void CalculateEstimateGoogleCloud()
        {
            Pages.GoogleCloudCalculator googleCloudCalculator = new Pages.GoogleCloudCalculator(_driver);
            EstimateGoogleCloudPrice estimatePrice = _config.Estimation;
            Tests.Assertions assertions = new Tests.Assertions(_driver);
            
            googleCloudCalculator.OpenPage(estimatePrice);
            googleCloudCalculator.AddToEstimate();
            googleCloudCalculator.SearchField(estimatePrice);
            googleCloudCalculator.SetServiceType(estimatePrice);
            googleCloudCalculator.AddNumberOfInstance(estimatePrice);
            googleCloudCalculator.ChoiceOperationSystem(estimatePrice);
            googleCloudCalculator.ChoiceProvisioningModel(estimatePrice);
            googleCloudCalculator.ChoiceMachineFamily(estimatePrice);
            googleCloudCalculator.ChoiceSeries(estimatePrice);
            googleCloudCalculator.ChoiceMachineType(estimatePrice);
            googleCloudCalculator.TurnAddGPUButton();
            googleCloudCalculator.ChoiceGPUModel(estimatePrice);
            googleCloudCalculator.ChoiceNumberOfGPUs(estimatePrice);
            googleCloudCalculator.ChoiceLocalSSD(estimatePrice);
            googleCloudCalculator.ChoiceRegion(estimatePrice);
            googleCloudCalculator.ChoiceCommitedUsage(estimatePrice);
            googleCloudCalculator.CountTotalEstimate();
            googleCloudCalculator.ShareEstimate();
            googleCloudCalculator.OpenEstimateSummary();
            googleCloudCalculator.SwitchBrowserWindow(1);
            assertions.VerifyInstanceNumber();
            assertions.VerifyOperationSystem();
            assertions.VerifyProvisioningModel();
            assertions.VerifyMachineType();
            assertions.VerifyGPUModel();
            assertions.VerifyNumberofGPU();
            assertions.VerifyLocalSSD();
            assertions.VerifyRegion();
            assertions.VerifyCommitedUsage();
            assertions.VerifyCurrency();
            assertions.VerifyTotalAmount();
            assertions.VerifyExploitaionTime();
        }
        public void SaveScreenshoot(string? screenshotName, string? folderPath)
        {
            try
            {
                Logger.Info("Generating of screenshot started.");
                ScreenshotTaker.TakeScreenshot(_driver, screenshotName, folderPath);
                Logger.Info("Generating of screenshot finished.");
            }
            catch (Exception ex)
            {
                Logger.Info($"Failed to capture screenshot. Exception message {ex.Message}");
                throw new Exception(ex.Message);
            }
        }

    }
}
